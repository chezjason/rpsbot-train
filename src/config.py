DATASET_NAME = "mix"

DATA_DIR = f"data/{DATASET_NAME}"
IMG_DIR = f"{DATA_DIR}/images"
NUMPY_DIR = f"{DATA_DIR}/numpy"

SUBSETS = ["train", "val", "test"]
CATEGORIES = ["rock", "paper", "scissors"]

CATEGORY_TO_ID = {
    "paper": 0,
    "rock": 1,
    "scissors": 2,
}

IMG_HEIGHT = 224
IMG_WIDTH = 224

NUM_AUG = 4

BATCH_SIZE = 32

MODEL_NAME = "mobilenetv2"
MODELS_DIR = f"models/{MODEL_NAME}"
SAVED_MODEL_DIR = f"{MODELS_DIR}/saved_model"
SAVED_MODEL_TRT_DIR = f"{MODELS_DIR}/saved_model_trt"

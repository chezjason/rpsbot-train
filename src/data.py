#!/usr/bin/env python3

"""Load image files and labels

This file contains the method that creates data and labels from a directory.
"""
import os
from pathlib import Path

import cv2
import numpy as np
from tensorflow.keras.preprocessing import image as keras_image
from tensorflow.keras.applications.mobilenet_v2 import preprocess_input
import imgaug as ia
import imgaug.augmenters as iaa
from imgaug.augmentables import Keypoint, KeypointsOnImage

import config as cfg


def augment_images(imgs):
    seq = iaa.Sequential([
        iaa.Fliplr(0.5),
        iaa.Affine(
            scale={"x": (0.8, 1.2), "y": (0.8, 1.2)}, # scale images to 80-120% of their size, individually per axis
            translate_percent={"x": (-0.2, 0.2), "y": (-0.2, 0.2)}, # translate by -20 to +20 percent (per axis)
            rotate=(-45, 45), # rotate by -45 to +45 degrees
            shear=(-16, 16), # shear by -16 to +16 degrees
        ),
        #iaa.GaussianBlur(sigma=(0, 3.0)),
        iaa.MultiplyBrightness((0.8, 1.2)),
        iaa.GammaContrast((0.5, 2.0)),
    ])
    imgs = seq(images=imgs)
    return imgs


def create_dataset(subset):
    images = []
    labels = []
    augid = 0
    for category in cfg.CATEGORIES:
        for subdir, _, fnames in os.walk(f"{cfg.IMG_DIR}/{subset}/{category}"):
            if fnames:
                for fname in fnames:
                    imgpath = f"{subdir}/{fname}"
                    #img = keras_image.load_img(imgpath, target_size=(IMAGE_HEIGHT, IMAGE_WIDTH))
                    img = cv2.imread(imgpath)
                    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
                    #if subset == "test":
                    #    img = img[:, 40:280, :]
                    img = cv2.resize(img, (cfg.IMAGE_HEIGHT, cfg.IMAGE_WIDTH))

                    if subset == "train":
                        print(f"[{augid:04d}] augmenting {fname}")
                        augid += 1
                        imgs = [img for _ in range(cfg.NUM_AUG)]
                        imgs = augment_images(imgs)
                        imgs.append(img)
                        lbls = [cfg.CATEGORY_TO_ID[category] for _ in range(cfg.NUM_AUG + 1)]
                    else:
                        imgs = [img]
                        lbls = [cfg.CATEGORY_TO_ID[category]]

                    imgs = [preprocess_input(np.array(img)) for img in imgs]
                    images.extend(imgs)
                    labels.extend(lbls)
                    
    images = np.array(images, dtype=np.float32)
    labels = np.array(labels, dtype=np.int32)

    np.savez(f"{cfg.NUMPY_DIR}/{subset}.npz", images=images, labels=labels)


def load_dataset(subset):
    loaded = np.load(f"{cfg.NUMPY_DIR}/{subset}.npz")
    train_images = loaded["images"]
    train_labels = loaded["labels"]    
    return train_images, train_labels


if __name__ == "__main__":
    for subset in cfg.SUBSETS:
        create_dataset(subset)

import os
import math
import random
import shutil


import numpy as np
import tensorflow as tf
from tensorflow.keras.preprocessing import image as keras_image
from tensorflow.keras.applications.mobilenet_v2 import preprocess_input
from keras.models import Model
from keras.layers import Input, Dense, Flatten, Dropout
from keras.applications.mobilenet_v2 import MobileNetV2
from keras.applications.resnet import ResNet50, ResNet152
from keras.applications.vgg16 import VGG16
from keras.applications.vgg19 import VGG19
from keras.applications.inception_v3 import InceptionV3
from sklearn.metrics import accuracy_score, precision_score, recall_score
import imgaug as ia
import imgaug.augmenters as iaa
from imgaug.augmentables import Keypoint, KeypointsOnImage
from tensorflow.python.compiler.tensorrt import trt_convert as trt

import config as cfg


class DataSequence(tf.keras.utils.Sequence):
    def __init__(self, subset, batch_size):
        self.subset = subset
        self.batch_size = batch_size

        self.all_data = []
        for category in cfg.CATEGORIES:
            for subdir, _, fnames in os.walk(f"{cfg.IMG_DIR}/{subset}/{category}"):
                if fnames:
                    for fname in fnames:
                        imgpath = f"{subdir}/{fname}"
                        self.all_data.append((imgpath, cfg.CATEGORY_TO_ID[category]))

        random.shuffle(self.all_data)

        print(f"{self.subset} dataset loaded:")
        print(f"\tsample: {self.all_data[0]}")
        print(f"\ttotal: {len(self.all_data)}")

    def __len__(self):
        return math.ceil(len(self.all_data) / self.batch_size)

    def __getitem__(self, idx):
        samples = self.all_data[idx * self.batch_size: (idx + 1) * self.batch_size]

        images = []
        labels = []
        for imgpath, lbl in samples:
            img = keras_image.load_img(imgpath, target_size=(cfg.IMG_HEIGHT, cfg.IMG_WIDTH))
            img = np.array(img)
            if self.subset == "train":
                img = self.augment_image(img)
            img = preprocess_input(img)

            images.append(img)
            labels.append(lbl)
        return np.array(images), np.array(labels)

    def augment_image(self, img):
        augseq = iaa.Sequential([
            iaa.Fliplr(0.5),
            iaa.Sometimes(
                0.9,
                iaa.Sequential([
                    iaa.Affine(
                        scale={"x": (0.8, 1.2), "y": (0.8, 1.2)},   # scale images to 80-120% of their size, individually per axis
                        translate_percent={"x": (-0.2, 0.2), "y": (-0.2, 0.2)},   # translate by -20 to +20 percent (per axis)
                        rotate=(-180, 180),   # rotate by -45 to +45 degrees
                        shear=(-16, 16),   # shear by -16 to +16 degrees
                    ),
                    #iaa.GaussianBlur(sigma=(0, 3.0)),
                    iaa.MultiplyBrightness((0.8, 1.2)),
                    iaa.GammaContrast((0.5, 2.0)),
                ]),
            ),
        ])
        return augseq(images=[img])[0]


def evaluate(model, eval_data):
    preds = [int(np.argmax(model.predict(img)[0])) for img, _ in eval_data]
    labels = [int(lbl[0]) for _, lbl in eval_data]
    print(preds[:20])
    #preds = np.argmax(preds, axis=1)
    acc = accuracy_score(labels, preds)
    prec = precision_score(labels, preds, average="weighted")
    rec = recall_score(labels, preds, average="weighted")

    print("labels: {}".format(labels))
    print("preds : {}".format(preds))
    print("accuracy: %f%%" % (acc * 100.))
    print("precision: %f%%" % (prec * 100.))
    print("recall: %f%%" % (rec * 100.))


def training():
    train_data = DataSequence("train", batch_size=cfg.BATCH_SIZE)
    val_data = DataSequence("val", batch_size=1)
    test_data = DataSequence("test", batch_size=1)
    test2_data = DataSequence("test2", batch_size=1)

    model = MobileNetV2(weights="imagenet", input_tensor=Input(shape=(cfg.IMG_HEIGHT, cfg.IMG_WIDTH, 3)))
    model = Model(inputs=model.inputs, outputs=model.layers[-2].output)
    for layer in model.layers:
        layer.trainable = False

    outputs = Dense(64, activation="swish")(model.output)
    outputs = Dropout(0.5)(outputs)
    outputs = Dense(3, activation="softmax")(outputs)
    model = Model(inputs=model.input, outputs=outputs)
    model.summary()
    
    model.compile(optimizer="adam", loss="sparse_categorical_crossentropy", metrics=["accuracy"])
    model.fit(x=train_data, y=None, batch_size=32, epochs=10, validation_data=val_data, shuffle=True)

    print("evaluation on val set:")
    evaluate(model, val_data)
    print("evaluation on test set:")
    evaluate(model, test_data)
    print("evaluation on test2 set:")
    evaluate(model, test2_data)

    return model


def export_models(model):
    if os.path.isdir(cfg.MODELS_DIR):
        shutil.rmtree(cfg.MODELS_DIR)

    model.save(cfg.SAVED_MODEL_DIR)

    converter = trt.TrtGraphConverterV2(input_saved_model_dir=cfg.SAVED_MODEL_DIR)
    converter.convert()
    converter.save(cfg.SAVED_MODEL_TRT_DIR)


if __name__ == "__main__":
    model = training()
    export_models(model)
